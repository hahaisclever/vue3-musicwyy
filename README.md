# musicapp

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### 项目还存在的问题：1.播放和暂停按钮不正确 2.自动播放问题 3.歌词已播放完的不会变回原来颜色
