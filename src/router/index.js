import { createRouter, createWebHistory } from "vue-router";
import Home from "../views/Home.vue";
import store from "@/store/index.js"

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/about",
    name: "About",
    component: () =>
      import( "../views/About.vue"),
  },
  {
    path: "/recommend",
    name: "Recommend",
    component: () =>
      import( "../views/Recommend.vue"),
  },
  {
    path: "/search",
    name: "Search",
    component: () =>
      import( "../views/Search.vue"),
  },
  {
    path: "/login",
    name: "Login",
    component: () =>
      import( "../views/Login.vue"),
  },
  {
    path: "/me",
    name: "Me",
    // 前置守卫
    beforeEnter: (to, from, next) => {
      // ...
      console.log(to, from, next);
      console.log(store.state.user);
      store.state.user.isLogin? next() : next('login')
    },
    component: () =>
      import( "../views/Me.vue"),
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
