import axios from 'axios'

const baseUrl = 'http://music.cpengx.cn'

// 获取轮播图
export function getBanner(type=0){
    return axios.get(`${baseUrl}/banner?type=${type}`)
}

// 首页图标
export function getIndexIcon(){
    return axios.get(`${baseUrl}/homepage/dragon/ball`)
}

// 每日推荐歌单
export function getMusicList(limit=10){
    return axios.get(`${baseUrl}/personalized?limit=${limit}`)
}

// 获取歌单详情
export function getPlayListDetail(id){
    return axios.get(`${baseUrl}/playlist/detail?id=${id}`)
}

// 获取歌词接口
export function getLyric(id){
    return axios.get(`${baseUrl}/lyric?id=${id}`)
}

// 搜索歌曲接口
export function searchMusic(keyword){
    return axios.get(`${baseUrl}/search?keywords=${keyword}`)
}

// 手机登录接口
export function phoneLogin(phone, password){
    return axios.get(`${baseUrl}/login/cellphone?phone=${phone}&password=${password}`)
}

// 获取用户详情接口
export function getUserDeatil(uid){
    return axios.get(`${baseUrl}/user/detail?uid=${uid}`)
}

export default {
    getBanner,
    getIndexIcon,
    getMusicList,
    getPlayListDetail,
    getLyric,
    searchMusic,
    phoneLogin,
    getUserDeatil,
}
