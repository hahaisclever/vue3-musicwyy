import { createStore } from "vuex";
import api from '@/api/index.js'

export default createStore({
  state: {
    playlist:[{ // 初始化数据
      id: 91453655,
      name: "如初",
      al:{
        id: 91453655,
        name: "如初",
        picUrl:"http://p3.music.126.net/viTq07NF-_wBrlPtVzqUcw==/109951165087160916.jpg",
        pic_str: "109951165087160916"
      }
    }],
    playCurrentIndex:0,
    lyric:'',
    currentTime:0, // 当前播放时间
    intervalId:0,
    user:{
      idLogin: false,
      userName: '张三',
      account:{},
      userDeatil:{},
    }
  },
  mutations: {
    setPlayList:(state,value)=>{
      state.playlist = value
    },
    pushPlayList:(state,value)=>{
      state.playlist.push(value)
    },
    setPlayIndex:(state,value)=>{
      state.playCurrentIndex = value
    },
    setLyric:(state,value)=>{
      state.lyric = value
    },
    setCurrentTime:(state,value)=>{
      state.currentTime = value
    },
    setUser:(state,value)=>{
      state.user = value
    },
  },
  getters:{
    lyricList:(state)=>{
      const arr = state.lyric?.split(/\n/igs).map((item)=>{
        let min = parseInt(item.slice(1,3))
        let sec = parseInt(item.slice(4,6))
        let mill = parseInt(item.slice(7,10))
        return {
          min,
          sec,
          mill,
          lyric:item.slice(11,item.length),
          content:item,
          time:mill+sec*1000+min*60*1000
        }
      })
      arr?.forEach((item,i) =>{
        if(i==0){
          item.pre = 0
        }else{
          item.pre = arr[i-1].time
        }
      })
      return arr
    }
  },
  actions: {

    // 异步获取歌词内容并保存
    async reqLyric(content,payload){
      const res = await api.getLyric(payload.id)
      content.commit('setLyric',res.data?.lrc?.lyric)
    },

    // 异步登录
    async phoneLogin(content,payload){
      const res = await api.phoneLogin(payload.phone,payload.password)
      if(res.data.code ==200){
        content.state.user.isLogin = true;
        content.state.user.account = res.data.account

        let userDetail = await api.getUserDeatil(res.data.account.id)
        content.state.user.userDeatil = userDetail.data
        localStorage.userData = JSON.stringify(content.state.user)
        content.commit('setUser',content.state.user)
      }
      return res
    }

  },
  modules: {},
});
